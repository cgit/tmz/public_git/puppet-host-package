#!/usr/bin/python

import glob
from distutils.core import setup
from puppethost import version, __name__

setup(name='puppet-host-package',
      version=version,
      description='Tool to create host specific package(s) for puppet hosts',
      author='Todd Zullinger',
      author_email='tmz+puppet@pobox.com',
      url='http://pobox.com/~tmz/puppethost/',
      license='GPLv2+',
      py_modules=['puppethost'],
      scripts=['puppet-host-package'],
      data_files=[('share/%s' % __name__, ['template.spec'])],
      long_description="""\
This tool generates a package containing the SSL certificate and other needed
files for a given host or hosts.""",
      classifiers=[
            'Development Status :: 4 - Beta',
            'Environment :: Console',
            'Intended Audience :: System Administrators',
            'License :: OSI Approved :: GNU General Public License (GPL)',
            'Natural Language :: English',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: System :: Systems Administration',
            ],
      )
