%global hostname        __HOSTNAME__
%global version         __VERSION__
%global release         __RELEASE__
%global ssldir          __SSLDIR__
%global pkgprefix       __PKGPREFIX__
%global puppetuser      __PUPPETUSER__

Name:           %{pkgprefix}%{hostname}
Version:        %{version}
Release:        %{release}
Summary:        Puppet SSL certificate files for %{hostname}

Group:          Applications/System
License:        Public Domain
Source0:        %{pkgprefix}%{hostname}-%{version}.tar.gz
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
Requires:       puppet

%description
Puppet SSL Keys for %{hostname}.

%prep
%setup -q


%build
# nothing to build


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{ssldir}/{private,public_keys}
cp -a * %{buildroot}%{ssldir}

# set modes - puppet resets these on each run, so there's no point in trying to
# tighten them up. :/
chmod 0771 %{buildroot}%{ssldir}
chmod 0770 %{buildroot}%{ssldir}/ca
chmod 0750 %{buildroot}%{ssldir}/private*
chmod 0600 %{buildroot}%{ssldir}/private_keys/%{hostname}.pem


%clean
rm -rf %{buildroot}


%files
%defattr(-,%{puppetuser},root,-)
%{ssldir}


%changelog
* Sat Jan 28 2012 Todd Zullinger <tmz@pobox.com>
- Replace %%define with %%global
- Use %%{puppetuser} for ownership of %%{ssldir}
- Tighten perms on ca dir to match puppet defaults

* Thu Nov 20 2008 Todd Zullinger <tmz@pobox.com>
- Initial template for puppet host package
